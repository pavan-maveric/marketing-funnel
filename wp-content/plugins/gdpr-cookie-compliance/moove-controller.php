<?php
if ( ! defined( 'ABSPATH' ) ) { exit; }
/**
 * Registering CONTROLLERS
 */
require_once dirname( __FILE__ ) . DIRECTORY_SEPARATOR . 'controllers' . DIRECTORY_SEPARATOR . 'moove-controller.php';
require_once dirname( __FILE__ ) . DIRECTORY_SEPARATOR . 'controllers' . DIRECTORY_SEPARATOR . 'moove-license-manager.php';
require_once dirname( __FILE__ ) . DIRECTORY_SEPARATOR . 'controllers' . DIRECTORY_SEPARATOR . 'moove-plugin-updater.php';
?>
