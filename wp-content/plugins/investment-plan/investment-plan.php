<?php
/*
    Plugin Name: Investment plan
    Plugin URI: 
    Description: Better investment plans for future
    Author: TFOCO
    Version: 1.0
     */
add_action('wp_enqueue_scripts', 'investment_styles');
add_action('wp_head', 'investment_script');
function investment_styles()
{
    wp_register_style('tfostyles', plugins_url('importer/vendor/Chart.min.css'));
    wp_enqueue_style('tfostyles');
}
function investment_script()
{
    wp_enqueue_script('tfo-chartjs', plugins_url('vendor/Chart.min.js', __FILE__), array('jquery'), null, true);
    wp_enqueue_script('tfo-jquery', plugins_url('tfojs/tfo.js', __FILE__), array('jquery'), null, true);
    ?>   
<?php
}
function investment_plan()
{
    echo '<canvas id="myChart"></canvas>';
    echo  '<div class="slidecontainer">
                <input type="range" min="5" max="15" value="5" class="slider1" id="myRange">
                <input type="range" min="1000000" max="10000000" value="1000000" class="slider1" id="myRange1">
                <input type="range" min="1000000" max="2000000" value="100000" class="slider1" id="myRange2">
                </div>';
}
function cf_shortcode()
{
    ob_start();
    investment_plan();
    return ob_get_clean();
}
add_shortcode('custom-post-list', 'cf_shortcode');

