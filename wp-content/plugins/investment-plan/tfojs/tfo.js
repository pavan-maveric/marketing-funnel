jQuery(document).ready(function($) {
  if ($("#myChart").length) {
    var ctx = document.getElementById("myChart").getContext("2d");
    defaultGraph(ctx, [0, 10, 5, 20, 15, 5, 60, 12, 80], []);
  }
  $("#myRange, #myRange1, #myRange2").on("change", function() {
    var ranges = {};
    ranges = getRanges($);
    $.ajax({
      url: "../wp-content/plugins/importer/test.php",
      type: "POST",
      data: ranges,
      success: function(res) {
        defaultGraph(ctx, [0, 10, 5, 20, 0, 50, 10, 17, 20], JSON.parse(res));
      }
    });
  });
});

/**
 * 
 * @param {Jquery event} $ 
 */

function getRanges($) {
  var range = {};
  range["range"] = $("#myRange").val();
  range["range1"] = $("#myRange1").val();
  range["range2"] = $("#myRange2").val();
  return range;
}


/**
 * 
 * @param { graph controller } ctx 
 * @param { Actual values of the graphs } actual 
 * @param { Results } results 
 */

function defaultGraph(ctx, actual, results) {
  var data = [
    {
      label: "Investments",
      backgroundColor: "#8F8F8F",
      borderColor: "#8F8F8F",
      fill: false,
      data: actual
    },
    {
      label: "Investments",
      backgroundColor: "#C2CE87",
      borderColor: "#C2CE87",
      fill: false,
      data: results
    }
  ];
  tfoChart(ctx, data);
}

/**
 * 
 * @param {controller of graph} ctx 
 * @param {Data points to display the graph} dataPoints 
 */
function tfoChart(ctx, dataPoints) {
  var chart = new Chart(ctx, {
    type: "line",
    data: {
      labels: ["2019", "2020", "2021", "2022", "2023"],
      datasets: dataPoints
    },
    options: {
      scales: {
        xAxes: [
          {
            gridLines: {
              drawOnChartArea: false
            }
          }
        ],
        yAxes: [
          {
            gridLines: {
              drawOnChartArea: false
            },
            scaleLabel: {
              //display: false,
            },
            ticks: {
              display: false
            }
          }
        ]
      }
    }
  });
  return chart;
}

