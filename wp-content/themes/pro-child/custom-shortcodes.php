<?php
add_action('wp_enqueue_scripts', 'register_slick_slider_scripts');

function register_slick_slider_scripts()
{
    wp_register_script('slick-slider-js', get_stylesheet_directory_uri().'/library/slick-slider/slick.min.js', ['jquery'], '1.1.1', true);
    wp_register_style('slick-slider-css', get_stylesheet_directory_uri().'/library/slick-slider/slick.css', [], '1.1.1');
    wp_register_style('slick-slider-theme-css', get_stylesheet_directory_uri().'/library/slick-slider/slick-theme.css', [], '1.1.1');
}

// Wealth Pillar Tiles - Homepage
function get_wealth_pillar_tiles($atts)
{
    extract(shortcode_atts([
        'post_count' => 4,
        'parent_page' => '123',
    ], $atts, 'wealth_planning_tiles'));

    $args = [
        'post_type' => 'page',
        'posts_per_page' => $post_count,
        'post_parent' => $parent_page,
        'order' => 'ASC',
        'orderby' => 'menu_order',
    ];

    $parent = new WP_Query($args);

    ob_start();
    include 'framework/views/integrity/wealth-pillar-tiles.php';
    $contents = ob_get_contents(); // data is now in here
    ob_end_clean();

    return $contents;
    // return get_the_post_thumbnail( $post_id, 'thumbnail','style=border-radius: 100%;' ).$testimonial_name .$testimonial_position.$testimonial_content;
}

// Investing Insights Category Posts Slider
function get_investing_insights_slider($atts)
{
    extract(shortcode_atts([
        'post_count' => 4,
        'per_row' => 4,
        'script_js' => '',
        'category' => 'investing-insights',
        'poster_size' => 'medium',
    ], $atts, 'investing_insights_slider'));

    $args = [
        'post_type' => 'post',
        'posts_per_page' => $post_count,
        'category_name' => $category,
        'order' => 'ASC',
        'orderby' => 'menu_order',
    ];

    $post_items = new WP_Query($args);

    ob_start();
    include 'framework/views/integrity/investing-insights-slider.php';
    $contents = ob_get_contents(); // data is now in here
    ob_end_clean();

    wp_enqueue_script('slick-slider-js');
    wp_enqueue_style('slick-slider-css');
    wp_enqueue_style('slick-slider-theme-css');

    wp_add_inline_script('slick-slider-js', "
    (function($){\"use strict\";
        $(document).ready(function(){
            $('.slick_items').slick({
                dots: true,
                infinite: false,
                speed: 300,
                slidesToShow: {$per_row},
                slidesToScroll: 1,
                responsive: [
                    {
                        breakpoint: 1024,
                        settings: {
                        slidesToShow: 3,
                        slidesToScroll: 1,
                        infinite: true,
                        dots: true
                        }
                    },
                    {
                        breakpoint: 600,
                        settings: {
                        slidesToShow: 2,
                        slidesToScroll: 2
                        }
                    },
                    {
                        breakpoint: 480,
                        settings: {
                        slidesToShow: 1,
                        slidesToScroll: 1
                        }
                    }
                  ]
              });
    });
    })(jQuery);
    ");

    return $contents;
}

add_shortcode('wealth_pillar_tiles', 'get_wealth_pillar_tiles');
add_shortcode('investing_insights_slider', 'get_investing_insights_slider');

// Removing Jetpack CSS
add_filter('jetpack_sharing_counts', '__return_false', 99);
add_filter('jetpack_implode_frontend_css', '__return_false', 99);

add_filter( 'gform_cdata_open', 'wrap_gform_cdata_open' );
function wrap_gform_cdata_open( $content = '' ) {
    $content = 'document.addEventListener( "DOMContentLoaded", function() { ';
    return $content;
}
add_filter( 'gform_cdata_close', 'wrap_gform_cdata_close' );
function wrap_gform_cdata_close( $content = '' ) {
    $content = ' }, false );';
    return $content;
}

add_action( 'admin_init', function() {
    add_post_type_support( 'available_position', 'page-attributes' );
} );


//Removing unnecessary scripts & styles in the homepage
function dequeue_homepage_scripts()
{
    // if this is not the contact page, remove the ihotspot script
    if (!is_page('contact')) {
        wp_dequeue_script('ihotspot-js');
    }

}

function dequeue_homepage_styles()
{
    // if this is not the contact page, remove the ihotspot script
    if (!is_page('contact')) {
        wp_deregister_style('ihotspot');
    }

    if (is_front_page()) {
        wp_deregister_style('themepunchboxextcss');
        wp_deregister_style('essential-grid-plugin-settings');
    }
}

// adjust priority to make sure this runs after the plugins add their scripts/styles
add_action('wp_enqueue_scripts', 'dequeue_homepage_scripts', 100);
add_action('wp_print_styles', 'dequeue_homepage_styles', 100);

add_filter('gform_pre_render_4', 'populate_posts');
add_filter('gform_pre_validation_4', 'populate_posts');
add_filter('gform_pre_submission_filter_4', 'populate_posts');
add_filter('gform_admin_pre_render_4', 'populate_posts');
function populate_posts($form)
{

    foreach ($form['fields'] as &$field) {

        if ($field->type != 'select' || strpos($field->cssClass, 'populate-positions') === false) {
            continue;
        }

        // you can add additional parameters here to alter the posts that are retrieved
        // more info: http://codex.wordpress.org/Template_Tags/get_posts
        // $posts = get_posts( 'numberposts=-1&post_status=publish' );

        $args = [
            'post_type' => 'available_position',
            'posts_per_page' => '-1',
            'post_status' => 'publish',
            'order' => 'ASC',
            'orderby' => 'menu_order',
        ];

        $availablePositions = new WP_Query($args);

        $choices = [];

        foreach ($availablePositions->posts as $position) {
            $choices[] = ['text' => $position->post_title, 'value' => $position->post_title];
        }

        // update 'Select a Post' to whatever you'd like the instructive option to be
        $field->placeholder = 'Select a Position';
        $field->choices = $choices;

    }

    return $form;
}

