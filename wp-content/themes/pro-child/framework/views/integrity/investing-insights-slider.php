<div id="investing_insights_slider" class="slick_items">
    <?php if ($post_items->have_posts()) :
        $i = 1;
        while ($post_items->have_posts()) : $post_items->the_post();
            ?>
            <div class="post_item post_item_<?= $i; ?>">
                <div class="item_container">
                    <div class="thumbnail">
                        <?php if (has_post_thumbnail()) {
                            the_post_thumbnail($poster_size);
                        } ?>
                    </div>

                    <div class="all-captions">
                        <h4 class="sl-post-title">
                            <a href="<?php the_permalink(); ?>"
                               tabindex="<?= $i; ?>"><?php the_title(); ?></a>
                        </h4>
                        <p><?php the_excerpt(); ?></p>
                        <ul class="sl-post-meta">
                            <li>
                                <time class="entry-date published updated"
                                      datetime="<?php the_time(); ?>">On
                                    <?php the_time('F j, Y'); ?></time>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
            <?php ++$i;
        endwhile;
    endif;
    wp_reset_postdata(); ?>

</div>