<div id="welcome_tiles" class="tiles_list">
    <ul>
        <?php if ($parent->have_posts()) : ?>

        <?php
        $i = 1;
        while ($parent->have_posts()) : $parent->the_post();

        ?>
        <li class="welcome_<?=$i; ?> tile_item"
            data-tile-mobile-bg-url="<?php the_field('thumbnail_background_mobile'); ?>"
            data-tile-bg-url="<?php the_field('thumbnail_background'); ?>">

            <h5 class="title"><?php the_field('tile_title'); ?>
            </h5>
            <div class="tile_content blurb">
                <?php the_field('tile_description'); ?>
            </div>
            <a href="<?php the_permalink(); ?>">
                <span class="tile_button"><?php the_field('call_to_action_button_label'); ?></span>
            </a>
        </li>
        <?php ++$i;
    endwhile; ?>

        <?php endif; wp_reset_postdata(); ?>
    </ul>
</div>